<?php

/**
 * The plugin bootstrap file
 *
 * @link              https://www.linkedin.com/in/drilons/
 * @since             1.0.0
 * @package           Products_List
 *
 * @wordpress-plugin
 * Plugin Name:       Products-List
 * Plugin URI:        ursaminor.dev
 * Description:       This is a plugin to display 3 oldest products. It requires woocommerce to be activated.
 * Version:           1.0.0
 * Author:            Drilon Shabani
 * Author URI:        https://www.linkedin.com/in/drilons/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       products-list
 */

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;

add_action( 'init', 'ds_products_list_init' );

function ds_products_list_init(){
    define('DS_PRODUCTS_LIST_PATH', plugin_dir_path(__FILE__));
    require( DS_PRODUCTS_LIST_PATH . '/shortcode.php' );
}