<?php 

function list_oldest_products(){

    if( !shortcode_exists('products') ) return "Please activate woocommerce. ";

    $out = '<div class="products-list">';

    $out .= do_shortcode("[products orderby='date' order='asc' limit='3']");

    $out .= '</div>';

    return $out;
}
add_shortcode('listOldestProducts', 'list_oldest_products'); 