# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

- Plugin to display 3 oldest products

### How do I get set up?

- You need woocommerce activated for this plugin to work

### How to use the shortcode

- The shortcode to display products is '[ listOldestProducts ]'

### Where is shortcode code placed

- The shortcode code is placed in shortcode.php file
